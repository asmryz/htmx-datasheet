const mongoose = require("mongoose");
const { Student } = require("./Student");
const { Course } = require("./Course");
const { Registration } = require("./Registration");
const { Grade } = require("./Grade");



(async () => {
    await mongoose.connect(`mongodb://127.0.0.1:27017/datasheet`);
})();

module.exports = {
    Student,
    Course,
    Grade, 
    Registration
};
